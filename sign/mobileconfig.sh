#!/bin/bash

openssl smime -sign -in $1 -out $2 -signer mbaike.crt -inkey mbaikenopass.key -certfile ca_bundle.pem -outform der -nodetach
