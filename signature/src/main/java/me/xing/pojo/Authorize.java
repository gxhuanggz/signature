package me.xing.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Authorize {

    private String csr;

    private String p8;

    private String iss;

    private String kid;


    public Authorize(String p8, String iss, String kid, String csr) {
        this.p8 = p8;
        this.iss = iss;
        this.kid = kid;
        this.csr = csr;
    }

    public Authorize(String p8, String iss, String kid) {
        this.p8 = p8;
        this.iss = iss;
        this.kid = kid;
    }


}
