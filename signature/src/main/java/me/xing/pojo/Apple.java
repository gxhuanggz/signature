package me.xing.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "开发者账号对象")
public class Apple {

  @ApiModelProperty(value = "id")
  private long id;

  @ApiModelProperty(value = "账号")
  private String account;

  @ApiModelProperty(value = "可用数量")
  private long count;

  @ApiModelProperty(value = "p8内容")
  private String p8;

  @ApiModelProperty(value = "iss")
  private String iss;

  @ApiModelProperty(value = "kid")
  private String kid;

  @ApiModelProperty(value = "p12文件地址")
  private String p12;

  @ApiModelProperty(value = "cerId")
  private String cerId;

  @ApiModelProperty(value = "开发者后台的通配证书id")
  private String bundleIds;

  @ApiModelProperty(value = "添加时间")
  private long create_time;

}
