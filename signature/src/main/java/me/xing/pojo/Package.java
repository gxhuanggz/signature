package me.xing.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "安装包对象")
public class Package {

  @ApiModelProperty(value = "id")
  private long id;

  @ApiModelProperty(value = "包名")
  private String name;

  @ApiModelProperty(value = "图标")
  private String icon;

  @ApiModelProperty(value = "版本")
  private String version;

  @ApiModelProperty(value = "编译版本号")
  private String buildVersion;

  @ApiModelProperty(value = "最小支持版本")
  private String miniVersion;

  @ApiModelProperty(value = "安装包id")
  private String bundleIdentifier;

  @ApiModelProperty(value = "简介")
  private String summary;

  @ApiModelProperty(value = "获取UDID证书地址")
  private String mobileconfig;

  @ApiModelProperty(value = "下载地址")
  private String link;

  @ApiModelProperty(value = "总下载量")
  private long count;

}
