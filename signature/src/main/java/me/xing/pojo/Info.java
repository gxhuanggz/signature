package me.xing.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Info {
    private String name;
    private String version;
    private String buildVersion;
    private String miniVersion;
    private String id;
    private String iconLink;
}
