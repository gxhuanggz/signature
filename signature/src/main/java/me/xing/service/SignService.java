package me.xing.service;

import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.lang.UUID;
import me.xing.core.common.BizErrorCode;
import me.xing.core.common.BizException;
import me.xing.core.config.Config;
import me.xing.pojo.Apple;
import me.xing.pojo.Authorize;
import me.xing.pojo.Device;
import me.xing.pojo.Package;
import me.xing.utils.FileUtils;
import me.xing.utils.ITSUtils;
import me.xing.utils.OSSUtils;
import me.xing.utils.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;


@Service
public class SignService {
    Logger logger = LoggerFactory.getLogger(SignService.class);
    @Autowired
    DeviceService deviceService;
    @Autowired
    AppleService appleService;
    @Autowired
    PackageService packageService;

    @Transactional(rollbackFor = Exception.class)
    public String analyzeUDID(String udid, String id) {
        String itemService = null;
        Device device = deviceService.getDeviceByUDID(udid);
        if (device == null) {
            // 设备不存在于任何帐号下
            logger.info("设备还没有签名");
            Apple apple = appleService.getBeUsableAppleAccount();
            if (apple == null) {
                // 没有找到合适的帐号
                logger.info("没有找到合适的帐号");
                throw new BizException(BizErrorCode.APPLE_DEVELOPER_UNFOUND);
            } else {
                // 找到合适的帐号
                Package pck = packageService.getPackageById(id);
                String resignature = insertDevice(udid, apple, pck);
                itemService = software(resignature, pck.getBundleIdentifier(), pck.getVersion(), pck.getName());
            }
        } else {
            // 设备存在
            Apple apple = appleService.getAppleAccountById(device.getAppleId());
            Package pck = packageService.getPackageById(id);
            String resignature = resignature(apple, device.getDeviceId(), pck);
            itemService = software(resignature, pck.getBundleIdentifier(), pck.getVersion(), pck.getName());
        }

        if (itemService != null) {
            packageService.updatePackageCountById(Integer.valueOf(id));
        }

        return itemService;
    }


    String insertDevice(String udid, Apple apple, Package pck) {
        logger.info("找到合适的帐号");
        // 发现可用账号
        String key = null;
        String devId = ITSUtils.insertDevice(udid, new Authorize(apple.getP8(), apple.getIss(), apple.getKid()));
        int i = deviceService.insertDevice(udid, apple.getId(), devId);
        if (i == 1) {
            appleService.updateAppleAccountDevicesCount(apple.getId());
            key = resignature(apple, devId, pck);
        }
        return key;
    }


    String resignature(Apple apple, String devId, Package pck) {
        String key = null;
        String classPath = FileUtils.getTempPath() + File.separator;
        String binPath=FileUtils.getBinPath()+File.separator;
        String objName = UUID.randomUUID().toString().replace("-", "");
        File outFile = new File(classPath + objName + ".ipa");

        long begin = System.currentTimeMillis();
        File mobileprovision = ITSUtils.insertProfile(apple, devId, classPath);
        long end = System.currentTimeMillis();
        long time = (end - begin) / 1000;
        logger.info("创建证书耗时: {}秒", time);
        String command1 = null;
        String command2 = null;
        String command3 = null;
        if (mobileprovision == null) {
            logger.info("文件创建失败");
        } else {
            logger.info("文件创建成功");
            String appUrl = Config.vpcAliMainHost + "/" + pck.getLink();
            File app = new File(classPath + pck.getLink());
            OSSUtils.download(pck.getLink(), app);
            logger.info("ipa下载完成: " + appUrl);
            String p12Url = Config.vpcAliMainHost + "/" + apple.getP12();
            File p12 = new File(classPath + apple.getP12());
            OSSUtils.download(apple.getP12(), p12);
            logger.info("p12下载完成: " + p12Url);
            String clientKey = classPath + "client.pem";
            String privateKey = classPath + "key.pem";
            command1 = "openssl pkcs12 -password pass:123456 -in " + p12.getAbsolutePath() + " -out " + clientKey + " -clcerts -nokeys";
            command2 = "openssl pkcs12 -password pass:123456 -in " + p12.getAbsolutePath() + " -out " + privateKey + " -nocerts -nodes";
            String  zsign=binPath+"zsign";
            // 调用本地shell脚本并传递必须参数
            ///usr/bin/isign   -c #{certificatePem} -k #{keyPem} -p #{mobileProvision} -o #{_outFile} #{inFile}
            command3 = zsign+" -c " + clientKey + " -k " + privateKey + " -m " +
                    mobileprovision.getAbsolutePath() + " -o " + outFile + " " + app.getAbsolutePath();
            logger.info("调用shell进行签名: {}", command3);
            try {
                begin = System.currentTimeMillis();
                Shell.run(command1);
                Shell.run(command2);
                boolean result = Shell.run(command3);
                end = System.currentTimeMillis();
                time = (end - begin) / 1000;
                logger.info("签名脚本执行耗时: {}秒", time);
                if (result) {
                    key = uploadIPA(outFile);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                logger.error(e.getMessage(), e);
            } finally {
                if (mobileprovision != null) {
                    mobileprovision.delete();
                }
                if (app != null) {
                    app.delete();
                }
                if (p12 != null) {
                    p12.delete();
                }
                if (outFile != null) {
                    outFile.delete();
                }

            }
        }
        return key;
    }

    String software(String ipaUrl, String id, String version, String title) {
        ipaUrl ="https://"+ Config.aliMainHost + "/" + ipaUrl;
        logger.info("ipaUrl: " + ipaUrl);
        String plist = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>  \n" +
                "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">  \n" +
                "<plist version=\"1.0\">  \n" +
                "<dict>  \n" +
                "    <key>items</key>  \n" +
                "    <array>  \n" +
                "        <dict>  \n" +
                "            <key>assets</key>  \n" +
                "            <array>  \n" +
                "                <dict>  \n" +
                "                    <key>kind</key>  \n" +
                "                    <string>software-package</string>  \n" +
                "                    <key>url</key>  \n" +
                "                    <string>" + ipaUrl + "</string>  \n" +
                "                </dict>  \n" +
                "            </array>  \n" +
                "            <key>metadata</key>  \n" +
                "            <dict>  \n" +
                "                <key>bundle-identifier</key>  \n" +
                "                <string>" + id + "</string>  \n" +
                "                <key>bundle-version</key>  \n" +
                "                <string>" + version + "</string>  \n" +
                "                <key>kind</key>  \n" +
                "                <string>software</string>  \n" +
                "                <key>title</key>  \n" +
                "                <string>" + title + "</string>  \n" +
                "            </dict>  \n" +
                "        </dict>  \n" +
                "    </array>  \n" +
                "</dict>  \n" +
                "</plist> ";
        String filePath = "itemService_" + UUID.randomUUID().toString().replace("-", "") + ".plist";
        FileWriter writer = new FileWriter(filePath);
        writer.write(plist);
        String itemService = uploadItemService(writer.getFile());
        writer.getFile().delete();
        return itemService;
    }


    String uploadItemService(File file) {
        String objName = UUID.randomUUID().toString().replace("-", "") + ".plist";
        OSSUtils.uploadFile(file, objName);
        return objName;
    }


    String uploadIPA(File file) {
        String objName = UUID.randomUUID().toString().replace("-", "") + ".ipa";
        logger.info("开始上传最终ipa文件, 文件名: " + objName);
        OSSUtils.uploadFile(file, objName);
        logger.info("文件上传完成");
        return objName;
    }


}
