package me.xing.service;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ZipUtil;
import me.xing.core.config.Config;
import me.xing.dao.PackageDao;
import me.xing.pojo.Info;
import me.xing.pojo.Package;
import me.xing.utils.OSSUtils;
import me.xing.utils.FileUtils;
import me.xing.utils.IpaUtils;
import me.xing.utils.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

/**
 * @author ：iizvv
 * @date ：Created in 2019-06-28 20:10
 * @description：IPA管理
 * @modified By：
 * @version: 1.0
 */
@Service
public class PackageService {
    Logger logger = LoggerFactory.getLogger(PackageService.class);

    @Autowired
    private PackageDao packageDao;

    public int insertPackage(Package pck) {
        return packageDao.insertPackage(pck);
    }

    public int updateMobileconfig(String mobileconfig, long id) {
        return packageDao.updateMobileconfig(mobileconfig, id);
    }

    public int updatePackage(Package pck) {
        return packageDao.updatePackage(pck);
    }

    public Package getPackageById(String id) {
        return packageDao.getPackageById(id);
    }

    public Package getPackageByBundleIdentifier(String bundleIdentifier) {
        return packageDao.getPackageByBundleIdentifier(bundleIdentifier);
    }

    public String getPackageLinkById(String id) {
        return packageDao.getPackageLinkById(id);
    }

    public int updatePackageCountById(long id) {
        return packageDao.updatePackageCountById(id);
    }

    public List<Package> getAllPackage() {
        return packageDao.getAllPackage();
    }

    @Transactional(rollbackFor = Exception.class)
    public int analyze(MultipartFile file, String summary) throws Exception {
        File icon = null;
        File excelFile = null;
        File ipa = null;
        File app = null;
        File infoFile = null;

        try {
            excelFile = File.createTempFile(UUID.randomUUID().toString(), ".ipa");
            file.transferTo(excelFile);
            ipa = ZipUtil.unzip(excelFile);
            app = IpaUtils.getAppFile(ipa);
            infoFile = new File(app.getAbsolutePath() + "/Info.plist");
            Info info = IpaUtils.getInfo(infoFile);
            String iconLink = info.getIconLink();
            String iconPath = app.getAbsolutePath() + "/" + iconLink;
            icon = new File(iconPath + "@3x.png");
            if (icon == null) {
                icon = new File(iconPath + "@2x.png");
            }
            iconLink = uploadIcon(icon);

            String appLink = uploadAppFile(excelFile);
            if (appLink != null) {
                logger.info("ipa文件上传完成");
            }
            Package pck = this.getPackageByBundleIdentifier(info.getId());
            if (pck != null) {
                pck.setName(info.getName());
                pck.setIcon(iconLink);
                pck.setVersion(info.getVersion());
                pck.setBuildVersion(info.getBuildVersion());
                pck.setMiniVersion(info.getMiniVersion());
                pck.setLink(appLink);
                if (summary != null) {
                    pck.setSummary(summary);
                }
                return this.updatePackage(pck);

            }
            pck = new Package();
            pck.setName(info.getName());
            pck.setIcon(iconLink);
            pck.setVersion(info.getVersion());
            pck.setBuildVersion(info.getBuildVersion());
            pck.setBundleIdentifier(info.getId());
            pck.setMiniVersion(info.getMiniVersion());
            pck.setLink(appLink);
            pck.setSummary(summary);
            this.insertPackage(pck);
            String mobileconfig = creatUDIDMobileconfig(pck.getId());
            if (mobileconfig != null) {
                return this.updateMobileconfig(mobileconfig, pck.getId());

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw  e;
        } finally {
            FileUtil.del(icon);
            FileUtil.del(excelFile);
            FileUtil.del(ipa);
            FileUtil.del(app);
            FileUtil.del(infoFile);
        }

        return 0;
    }


    private String uploadAppFile(File file) {
       logger.info("开始上传原始ipa文件");
        String objName = UUID.randomUUID().toString().replace("-", "") + ".ipa";
        OSSUtils.uploadFile(file, objName);
        return objName;
    }

    private String uploadIcon(File file) {
        String objName = UUID.randomUUID().toString().replace("-", "") + ".png";
        OSSUtils.uploadFile(file, objName);
        return objName;
    }


    private String creatUDIDMobileconfig(long id) throws Exception{
        logger.info("创建获取UDID所用证书");
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n" +
                "<plist version=\"1.0\">\n" +
                "    <dict>\n" +
                "        <key>PayloadContent</key>\n" +
                "        <dict>\n" +
                "            <key>URL</key>\n" +
                "            <string>" + Config.udidURL + "/udid/getUDID?id=" + id + "</string> <!--接收数据的接口地址-->\n" +
                "            <key>DeviceAttributes</key>\n" +
                "            <array>\n" +
                "                <string>SERIAL</string>\n" +
                "                <string>MAC_ADDRESS_EN0</string>\n" +
                "                <string>UDID</string>\n" +
                "                <string>IMEI</string>\n" +
                "                <string>ICCID</string>\n" +
                "                <string>VERSION</string>\n" +
                "                <string>PRODUCT</string>\n" +
                "            </array>\n" +
                "        </dict>\n" +
                "        <key>PayloadOrganization</key>\n" +
                "        <string>" + Config.payloadOrganization + "</string>  <!--组织名称-->\n" +
                "        <key>PayloadDisplayName</key>\n" +
                "        <string>" + Config.payloadDisplayName + "</string>  <!--安装时显示的标题-->\n" +
                "        <key>PayloadVersion</key>\n" +
                "        <integer>1</integer>\n" +
                "        <key>PayloadUUID</key>\n" +
                "        <string>" + UUID.randomUUID().toString().replace("-", "") + "</string>  <!--自己随机填写的唯一字符串-->\n" +
                "        <key>PayloadIdentifier</key>\n" +
                "        <string>online.iizvv.profile-service</string>\n" +
                "        <key>PayloadDescription</key>\n" +
                "        <string>" + Config.payloadDescription + "</string>   <!--描述-->\n" +
                "        <key>PayloadType</key>\n" +
                "        <string>Profile Service</string>\n" +
                "    </dict>\n" +
                "</plist>";


        String tempName = FileUtils.getSignDir() + File.separator + "udid_" + id + "_" + UUID.randomUUID().toString().replace("-", "");
        String tempMobileconfig = tempName + ".mobileconfig";
        FileWriter writer = new FileWriter(tempMobileconfig);
        writer.write(xml);
        logger.info("开始执行shell");
        String signMobileConfig = tempName + "_.mobileconfig";

        // openssl smime -sign -in Example.mobileconfig -out SignedVerifyExample.mobileconfig -signer InnovCertificates.pem -certfile root.crt.pem -outform der -nodetach
        //String com="openssl smime -sign -in "+ writer.getFile().getAbsolutePath()+" -out "+signMobileConfig+" -signer "+ clientPem+" -certfile "+rootCertPem+" -outform der -nodetach";
        String com = FileUtils.getSignShellRealPath() + " " + writer.getFile().getAbsolutePath() + " " + signMobileConfig+" "+FileUtils.getSignDir();
        File file=null;
        try {
            Shell.run(com);
            logger.info("shell执行成功, 文件位置为: " + signMobileConfig);
            file= new File(signMobileConfig);
            signMobileConfig = uploadMobileconfig(file);

        } catch (Exception e) {
            logger.error("shell执行失败", e);
            signMobileConfig = uploadMobileconfig(writer.getFile());
            throw e;
        } finally {

            writer.getFile().delete();
            if(file!=null) {
                file.delete();
            }

        }
        logger.info("mobileconfig文件上传结束");
        return signMobileConfig;
    }


    private String uploadMobileconfig(File file) {
        String objName = UUID.randomUUID().toString().replace("-", "") + ".mobileconfig";
        OSSUtils.uploadFile(file, objName);
        return objName;
    }


}
