package me.xing.utils;

import me.xing.core.config.Config;
import org.springframework.boot.system.ApplicationHome;

import java.io.File;

public class FileUtils {


    public static String getSuffixName(File file) {
        String fileName = file.getName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        return suffix;
    }

    public static String getJarClassPath() {
        ApplicationHome h = new ApplicationHome(FileUtils.class);
        File jarF = h.getSource();
        return jarF.getParentFile().toString();
    }

    public static String getTempPath() {
        return getJarClassPath() + File.separator + Config.TRMP;
    }

    public static String getBinPath() {
        return getJarClassPath() + File.separator + Config.BIN;
    }

    public static String getSignShellRealPath() {
        return getSignDir() + File.separator + Config.MOBLILE_CONFIG_SH;

    }

    public static String getSignDir() {
        return getJarClassPath() + File.separator + Config.SIGN_DIR;
    }


}
