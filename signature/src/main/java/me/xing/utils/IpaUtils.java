package me.xing.utils;

import cn.hutool.core.io.file.FileReader;
import com.dd.plist.NSDictionary;
import com.dd.plist.PropertyListParser;
import me.xing.pojo.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

public class IpaUtils {

    private static Logger logger = LoggerFactory.getLogger(IpaUtils.class);

    public static File getAppFile(File ipaFile) {
        File payload = new File(ipaFile.getAbsolutePath() + "/Payload/");
        if (payload != null) {
            for (File file : payload.listFiles()) {
                logger.info("文件的后缀为：{}", FileUtils.getSuffixName(file));
                if (FileUtils.getSuffixName(file).equalsIgnoreCase("app")) {
                    return file;
                }
            }
        }
        return null;
    }

    public static Info getInfo(File plist) throws Exception {
        Info info = new Info();
        NSDictionary parse = (NSDictionary) PropertyListParser.parse(new FileReader(plist).readBytes());
        String name = parse.get("CFBundleName").toString();
        if (parse.containsKey("CFBundleDisplayName")) {
            name = parse.get("CFBundleDisplayName").toString();
        }
        info.setName(name);
        String version = parse.get("CFBundleShortVersionString").toString();
        info.setVersion(version);
        String buildVersion = parse.get("CFBundleVersion").toString();
        info.setBuildVersion(buildVersion);
        String miniVersion = parse.get("MinimumOSVersion").toString();
        info.setMiniVersion(miniVersion);
        String id = parse.get("CFBundleIdentifier").toString();
        info.setId(id);
        NSDictionary icons = null;
        if (parse.containsKey("CFBundleIcons")) {
            icons = (NSDictionary) parse.get("CFBundleIcons");
        } else if (parse.containsKey("CFBundleIcons~ipad")) {
            icons = (NSDictionary) parse.get("CFBundleIcons~ipad");
        }
        String iconLink = null;
        if (icons.toJavaObject() != null) {
            List list = ((NSDictionary) icons.get("CFBundlePrimaryIcon")).get("CFBundleIconFiles").toJavaObject(List.class);
            iconLink = (String) list.get(list.size() - 1);

        }
        info.setIconLink(iconLink);
        return info;

    }





}
