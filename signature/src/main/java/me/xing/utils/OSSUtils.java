package me.xing.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.*;
import me.xing.core.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class OSSUtils {
    private static Logger logger = LoggerFactory.getLogger(OSSUtils.class);

    public static void uploadPartFile(File file, String objName) {
        OSS ossClient = null;
        try {
            String bucket = Config.aliMainBucket;
            ossClient = new OSSClientBuilder().build(Config.vpcEndpoint, Config.accessKeyID, Config.accessKeySecret);
            InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucket, objName);
            InitiateMultipartUploadResult result = ossClient.initiateMultipartUpload(request);
            String uploadId = result.getUploadId();
            List<PartETag> partETags = new ArrayList<PartETag>();
            final long partSize = 4 * 1024 * 1024L;
            long fileLength = file.length();
            int partCount = (int) (fileLength / partSize);
            if (fileLength % partSize != 0) {
                partCount++;
            }
            for (int i = 0; i < partCount; i++) {
                long startPos = i * partSize;
                long curPartSize = (i + 1 == partCount) ? (fileLength - startPos) : partSize;
                InputStream instream = new FileInputStream(file);
                // 跳过已经上传的分片。
                instream.skip(startPos);
                UploadPartRequest uploadPartRequest = new UploadPartRequest();
                uploadPartRequest.setBucketName(bucket);
                uploadPartRequest.setKey(objName);
                uploadPartRequest.setUploadId(uploadId);
                uploadPartRequest.setInputStream(instream);
                // 设置分片大小。除了最后一个分片没有大小限制，其他的分片最小为100KB。
                uploadPartRequest.setPartSize(curPartSize);
                // 设置分片号。每一个上传的分片都有一个分片号，取值范围是1~10000，如果超出这个范围，OSS将返回InvalidArgument的错误码。
                uploadPartRequest.setPartNumber(i + 1);
                // 每个分片不需要按顺序上传，甚至可以在不同客户端上传，OSS会按照分片号排序组成完整的文件。
                UploadPartResult uploadPartResult = ossClient.uploadPart(uploadPartRequest);
                // 每次上传分片之后，OSS的返回结果会包含一个PartETag。PartETag将被保存到partETags中。
                partETags.add(uploadPartResult.getPartETag());

            }
            Collections.sort(partETags, new Comparator<PartETag>() {
                public int compare(PartETag p1, PartETag p2) {
                    return p1.getPartNumber() - p2.getPartNumber();
                }
            });
            CompleteMultipartUploadRequest completeMultipartUploadRequest =
                    new CompleteMultipartUploadRequest(bucket, objName, uploadId, partETags);
            ossClient.completeMultipartUpload(completeMultipartUploadRequest);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);

        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }

    }

    public static void uploadFile(File file, String objName) {
        OSS ossClient = null;
        try {
            String bucket = Config.aliMainBucket;
            ossClient = new OSSClientBuilder().build(Config.vpcEndpoint, Config.accessKeyID, Config.accessKeySecret);
            InputStream is = new FileInputStream(file);
            ossClient.putObject(bucket, objName, is);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }

    }

    public static void uploadFile(byte[] bytes, String objName) {
        OSS ossClient = null;
        try {
            String bucket = Config.aliMainBucket;
            ossClient = new OSSClientBuilder().build(Config.vpcEndpoint, Config.accessKeyID, Config.accessKeySecret);
            ossClient.putObject(bucket, objName, new ByteArrayInputStream(bytes));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }

    }

    public static void download(String objectName, File dest) {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = Config.vpcEndpoint;
// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        String accessKeyId = Config.accessKeyID;
        String accessKeySecret = Config.accessKeySecret;
        String bucketName = Config.aliMainBucket;


// 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

// 下载OSS文件到本地文件。如果指定的本地文件存在会覆盖，不存在则新建。
        ossClient.getObject(new GetObjectRequest(bucketName, objectName), dest);

// 关闭OSSClient。
        ossClient.shutdown();
    }


}
