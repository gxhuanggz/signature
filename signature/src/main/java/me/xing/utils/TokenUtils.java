package me.xing.utils;

import cn.hutool.core.codec.Base64;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import me.xing.pojo.Authorize;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

@Slf4j
public class TokenUtils {

    static String getToken(Authorize authorize) {
        String p8 = authorize.getP8();
        String iss = authorize.getIss();
        String kid = authorize.getKid();
        String s = p8.
                replace("-----BEGIN PRIVATE KEY-----", "").
                replace("-----END PRIVATE KEY-----", "");
        byte[] encodeKey = Base64.decode(s);
        Key key = new SecretKeySpec(encodeKey,
                SignatureAlgorithm.ES256.getJcaName());
        String token = Jwts.builder().
                setHeaderParam(JwsHeader.ALGORITHM, "ES256").
                setHeaderParam(JwsHeader.KEY_ID, kid).
                setHeaderParam(JwsHeader.TYPE, "JWT").
                setIssuer(iss).
                claim("exp", System.currentTimeMillis() / 1000 + 60 * 10).
                setAudience("appstoreconnect-v1").
                signWith(SignatureAlgorithm.ES256, key).
                compact();


        return token;
    }
}
