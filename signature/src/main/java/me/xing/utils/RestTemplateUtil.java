package me.xing.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate 工具类
 *
 * @author: author
 * @create: 2019-06-05 14:06
 **/
@Slf4j
public class RestTemplateUtil {

    private static class SingletonRestTemplate {
        static final RestTemplate INSTANCE = new RestTemplate();
    }

    private RestTemplateUtil() {
    }


    public static RestTemplate getInstance() {
        return SingletonRestTemplate.INSTANCE;
    }

    /**
     * 发送POST请求
     *
     * @param url        请求url
     * @param returnType 返回类型,必须重写toString()方法,否则不能正确记录日志信息
     * @return 指定的返回类型
     */
    public static final <T> T doPostByJson(String url, Class<T> returnType, String token) {
        return doPostByJson(url, null, returnType, token);
    }

    /**
     * 发送POST请求
     *
     * @param url        请求url
     * @param data       发送的数据,必须重写toString()方法,否则不能正确记录日志信息
     * @param returnType 返回类型,必须重写toString()方法,否则不能正确记录日志信息
     * @return 指定的返回类型
     */
    public static final <T, E> T doPostByJson(String url, E data, Class<T> returnType, String token) {
        return doPost(url, data, MediaType.APPLICATION_JSON_UTF8, returnType, token);
    }

    /**
     * 发送POST请求
     *
     * @param url        请求url
     * @param data       发送的数据,必须重写toString()方法,否则不能正确记录日志信息
     * @param returnType 返回类型,必须重写toString()方法,否则不能正确记录日志信息
     * @return 指定的返回类型
     */
    public static final <T> T doPostByFormData(String url, MultiValueMap<String, String> data, Class<T> returnType, String token) {
        return doPost(url, data, MediaType.APPLICATION_FORM_URLENCODED, returnType, token);
    }

    /**
     * 发送GET请求
     *
     * @param url   请求url
     * @param clazz 返回类型,必须重写toString()方法,否则不能正确记录日志信息
     * @return 指定的返回类型
     */
    public static final <T> T doGet(String url, Class<T> clazz, String token) {
        log.info("GET_REQUEST: {}, {}", url, clazz.getName());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Encoding", "UTF-8");
        headers.add("Content-Type", "application/json; charset=UTF-8");
        if (token != null) {
            headers.add("Authorization", "Bearer " + token);
        }

        T result = getInstance().getForObject(url, clazz);
        log.info("GET_RESPONSE: {}", result);

        return result;
    }

    /**
     * 发送GET请求
     *
     * @param url 请求url
     * @return 指定的返回类型
     */
    public static final void doDelete(String url, String token) {
        log.info("GET_REQUEST: {}, {}", url, token);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Encoding", "UTF-8");
        headers.add("Content-Type", "application/json; charset=UTF-8");
        if (token != null) {
            headers.add("Authorization", "Bearer " + token);
        }

        getInstance().delete(url);


    }


    /**
     * 发送POST请求
     *
     * @param url         请求url
     * @param data        发送的数据,必须重写toString()方法,否则不能正确记录日志信息
     * @param requestType 请求头类型
     * @param returnType  返回类型,必须重写toString()方法,否则不能正确记录日志信息
     * @return 指定的返回类型
     */
    public static final <T, E> T doPost(String url, E data, MediaType requestType, Class<T> returnType, String token) {
        log.info("POST_REQUEST: {}, {}, {}, {}", url, data, requestType, returnType.getName());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(requestType);
        headers.add("Accept", "application/json");
        headers.add("Content-Encoding", "UTF-8");
        if (token != null) {
            headers.add("Authorization", "Bearer " + token);
        }
        HttpEntity<E> entity = new HttpEntity<>(data, headers);

        T result = getInstance().postForObject(url, entity, returnType);

        log.info("POST_RESPONSE: {}", result);
        return result;
    }


}
