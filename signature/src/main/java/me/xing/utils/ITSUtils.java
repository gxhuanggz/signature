package me.xing.utils;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.lang.UUID;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import me.xing.core.config.Config;
import me.xing.pojo.Apple;
import me.xing.pojo.Authorize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ITSUtils {
    static Logger logger = LoggerFactory.getLogger(ITSUtils.class);
    public final static String DEVICES_URL = "https://api.appstoreconnect.apple.com/v1/devices";
    public final static String PROFILE_URL = "https://api.appstoreconnect.apple.com/v1/profiles";
    public final static String CERTIFICATES_URL = "https://api.appstoreconnect.apple.com/v1/certificates";
    public final static String BUNDLEIDS_URL = "https://api.appstoreconnect.apple.com/v1/bundleIds";


    public static Map getNumberOfAvailableDevices(Authorize authorize) {
        String token = TokenUtils.getToken(authorize);
        Map map = RestTemplateUtil.doGet(DEVICES_URL, Map.class, token);
        JSONArray data = (JSONArray) map.get("data");
        List devices = new LinkedList();
        for (Object datum : data) {
            Map device = new HashMap();
            Map m = (Map) datum;
            String id = (String) m.get("id");
            Map attributes = (Map) m.get("attributes");
            String udid = (String) attributes.get("udid");
            device.put("deviceId", id);
            device.put("udid", udid);
            devices.add(device);
        }
        removeCertificates(token);
        removeBundleIds(token);
        String cerId = insertCertificates(token, authorize.getCsr());
        String bundleIds = insertBundleIds(token);
        Map meta = (Map) map.get("meta");
        Map paging = (Map) meta.get("paging");
        int total = (int) paging.get("total");
        Map res=new HashMap();
        res.put("number", Config.total - total);
        res.put("devices", devices);
        res.put("cerId", cerId);
        res.put("bundleIds", bundleIds);
        return res;
    }


    public static String insertDevice(String udid, Authorize authorize) {
        String token = TokenUtils.getToken(authorize);
        Map body = new HashMap();
        body.put("type", "devices");
        Map attributes = new HashMap();
        attributes.put("name", udid);
        attributes.put("udid", udid);
        attributes.put("platform", "IOS");
        body.put("attributes", attributes);
        Map data = new HashMap();
        data.put("data", body);
        Map map = RestTemplateUtil.doPostByJson(DEVICES_URL,Map.class,token);
        Map data1 = (Map) map.get("data");
        String id = (String) data1.get("id");
        return id;
    }


    public static File insertProfile(Apple apple, String devId, String path) {
        Map body = new HashMap();
        body.put("type", "profiles");
        String name = UUID.randomUUID().toString().replace("-", "");
        Map attributes = new HashMap();
        attributes.put("name", name);
        attributes.put("profileType", "IOS_APP_ADHOC");
        body.put("attributes", attributes);
        Map relationships = new HashMap();
        Map bundleId = new HashMap();
        Map data2 = new HashMap();
        data2.put("id", apple.getBundleIds());
        data2.put("type", "bundleIds");
        bundleId.put("data", data2);
        relationships.put("bundleId", bundleId);
        Map certificates = new HashMap();
        Map data3 = new HashMap();
        data3.put("id", apple.getCerId());
        data3.put("type", "certificates");
        List list = new LinkedList();
        list.add(data3);
        certificates.put("data", list);
        relationships.put("certificates", certificates);
        Map devices = new HashMap();
        Map data4 = new HashMap();
        data4.put("id", devId);
        data4.put("type", "devices");
        List list2 = new LinkedList();
        list2.add(data4);
        devices.put("data", list2);
        relationships.put("devices", devices);
        body.put("relationships", relationships);
        Map data = new HashMap();
        data.put("data", body);


        Authorize authorize = new Authorize();
        authorize.setP8(apple.getP8());
        authorize.setIss(apple.getIss());
        authorize.setKid(apple.getKid());
        String token = TokenUtils.getToken(authorize);
        Map map = RestTemplateUtil.doPostByJson(PROFILE_URL,Map.class,token);
        Map o = (Map) map.get("data");
        Map o2 = (Map) o.get("attributes");
        String profileContent = (String) o2.get("profileContent");
        File file = base64ToFile(profileContent, path + name + ".mobileprovision");
        return file;
    }


    static void removeCertificates(String token) {

        Map map = RestTemplateUtil.doGet(CERTIFICATES_URL, Map.class, token);
        JSONArray data = (JSONArray) map.get("data");
        for (Object datum : data) {
            Map m = (Map) datum;
            String id = (String) m.get("id");
            RestTemplateUtil.doDelete(CERTIFICATES_URL + "/" + id, token);
        }
    }


    static void removeBundleIds(String token) {

        Map map = RestTemplateUtil.doGet(BUNDLEIDS_URL, Map.class, token);
        logger.info("查询bundleIds，结果：{}", map);
        JSONArray data = (JSONArray) map.get("data");
        for (Object datum : data) {
            Map m = (Map) datum;
            String id = (String) m.get("id");
            RestTemplateUtil.doDelete(BUNDLEIDS_URL + "/" + id, token);

        }
    }


    static File base64ToFile(String base64, String fileName) {
        File file = null;
        BufferedOutputStream bos = null;
        java.io.FileOutputStream fos = null;
        try {
            byte[] bytes = Base64.decode(base64);
            file = new File(fileName);
            fos = new java.io.FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bytes);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return file;
    }

    static String insertCertificates(String token, String csr) {

        Map body = new HashMap();
        body.put("type", "certificates");
        Map attributes = new HashMap();
        attributes.put("csrContent", csr);
        attributes.put("certificateType", "IOS_DISTRIBUTION");
        body.put("attributes", attributes);
        Map data = new HashMap();
        data.put("data", body);
        Map map = RestTemplateUtil.doPostByJson(CERTIFICATES_URL, Map.class, token);
        logger.info("申请开发者证书，结果:{}", map);
        Map data1 = (Map) map.get("data");
        String id = (String) data1.get("id");
        return id;
    }

    /**
     * create by: iizvv
     * description: 创建BundleIds
     * create time: 2019-07-01 15:00
     *
     * @return id
     */
    static String insertBundleIds(String token) {

        Map body = new HashMap();
        body.put("type", "bundleIds");
        Map attributes = new HashMap();
        attributes.put("identifier", "com.seedPlantTogether.game");
        attributes.put("name", "AppBundleId");
        attributes.put("platform", "IOS");
        body.put("attributes", attributes);
        Map data = new HashMap();
        data.put("data", body);
        Map map = RestTemplateUtil.doPostByJson(BUNDLEIDS_URL, Map.class, token);
        logger.info("申请开发者bundleIds，结果:{}", map);
        Map data1 = (Map) map.get("data");
        String id = (String) data1.get("id");
        return id;
    }


}
