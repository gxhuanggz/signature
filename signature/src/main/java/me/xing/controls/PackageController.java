package me.xing.controls;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.xing.core.config.Config;
import me.xing.core.pojo.Result;
import me.xing.pojo.Package;
import me.xing.service.PackageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author ：iizvv
 * @date ：Created in 2019-06-28 15:08
 * @description：IPA包管理
 * @version: 1.0
 */
@RestController
@RequestMapping("/package")
@Api(tags = {"IPA包管理"})
public class PackageController {
    Logger logger = LoggerFactory.getLogger(PackageController.class);

    @Autowired
    private PackageService packageService;

    @ApiOperation(value = "/getAllPackage", notes = "获取全部IPA")
    @GetMapping("/getAllPackage")
    public Result<List<Package>> getAllPackage() {
        List<Package> allPackage = packageService.getAllPackage();
        allPackage.forEach(p -> {
            p.setIcon(Config.aliMainHost + "/" + p.getIcon());
            p.setMobileconfig(Config.aliMainHost + "/" + p.getMobileconfig());
        });
        return Result.ok(allPackage);
    }


    @ApiOperation(value = "/uploadPackage", notes = "上传ipa", produces = "application/json")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "file", value = "ipa文件", required = true),
            @ApiImplicitParam(name = "summary", value = "简介")
    })
    @PostMapping("/uploadPackage")
    public Result uploadPackage(MultipartFile file, String summary) {
        if (file != null) {
            String fileName = file.getOriginalFilename();
            String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (suffix.equalsIgnoreCase("ipa")) {
                // 上传的文件为ipa文件
                try {
                    int i = this.packageService.analyze(file, summary);
                    if (i == 1) {
                        return Result.ok("ipa文件提交成功");

                    } else {
                        return Result.error("ipa文件解析失败");
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    return Result.error("ipa解析失败");
                }
            } else {
                // 上传的文件非ipa文件
                return Result.error("请检查文件类型");
            }
        } else {
            return Result.error("文件不存在， 检查路径是否正确");
        }

    }

    @ApiOperation(value = "/getPackageById", notes = "获取指定ipa")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "ipaId", required = true)
    })
    @GetMapping("/getPackageById")
    public Result getPackageById(String id) {
        Package pck = packageService.getPackageById(id);
        if (pck == null) {
            return Result.error("内容不存在");
        } else {
            pck.setIcon(Config.aliMainHost + "/" + pck.getIcon());
            pck.setMobileconfig(Config.aliMainHost + "/" + pck.getMobileconfig());
            return Result.ok(pck);
        }

    }


}
