package me.xing.controls;

import com.dd.plist.NSDictionary;
import com.dd.plist.PropertyListParser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.xing.core.config.Config;
import me.xing.service.PackageService;
import me.xing.service.SignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

/**
 * @author ：iizvv
 * @date ：Created in 2019-06-26 20:39
 * @description：获取UDID
 * @modified By：
 * @version: 1.0
 */
@RestController
@Api(tags = {"获取UDID下载APP"})
@RequestMapping("/udid")
public class UDIDController {
    Logger logger = LoggerFactory.getLogger(UDIDController.class);

    @Autowired
    SignService signService;



    @ApiOperation(value = "/getUDID", notes = "获取设备udid", produces = "application/json")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "IPA id", required = true),
    })
    @PostMapping("/getUDID")
    public void getUDID(HttpServletResponse response, HttpServletRequest request, String id) throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        long begin = System.currentTimeMillis();
        String ua = request.getHeader("User-Agent");
        logger.info("当前产品id：{} ,当前用户User-Agent:{} ", id, ua);
        String itemService = null;

        request.setCharacterEncoding("UTF-8");
        //获取HTTP请求的输入流
        InputStream is = request.getInputStream();
        //已HTTP请求输入流建立一个BufferedReader对象
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        //读取HTTP请求内容
        String buffer = null;
        while ((buffer = br.readLine()) != null) {
            sb.append(buffer);
        }
        String xml = sb.toString().substring(sb.toString().indexOf("<?xml"), sb.toString().indexOf("</plist>") + 8);
        NSDictionary parse = (NSDictionary) PropertyListParser.parse(xml.getBytes());
        String udid = (String) parse.get("UDID").toJavaObject();
        itemService = signService.analyzeUDID(udid, id);
        logger.info("itemService文件名为: " + itemService);
        String encode = "itms-services://?action=download-manifest&url=https://" + Config.aliMainHost + "/" + itemService;
        long end = System.currentTimeMillis();
        long result = (end - begin) / 1000;
        logger.info("自动签名执行耗时: " + result + "秒");
        response.setHeader("Location", encode);
        response.setStatus(301);
    }


}
