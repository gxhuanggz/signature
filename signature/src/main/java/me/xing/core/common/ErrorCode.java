package me.xing.core.common;

public interface ErrorCode {
    /**
     * 获取错误码
     * @return
     */
    Integer getCode();

    /**
     * 获取错误信息
     * @return
     */
    String getDescription();

}
