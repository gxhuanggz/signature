package me.xing.core.common;

public enum BizErrorCode implements ErrorCode {
    /**
     * 未指明的异常
     */
    UNSPECIFIED(500, "网络异常，请稍后再试"),
    APPLE_DEVELOPER_UNFOUND(1005,"没有找到合适的帐号")
    ;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 描述
     */
    private String description;

    /**
     * @param code        错误码
     * @param description 描述
     */
    private BizErrorCode(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static BizErrorCode getByCode(Integer code) {
        for (BizErrorCode value : BizErrorCode.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return UNSPECIFIED;
    }

    /**
     * 枚举是否包含此code
     *
     * @param code 枚举code
     * @return 结果
     */
    public static Boolean contains(Integer code) {
        for (BizErrorCode value : BizErrorCode.values()) {
            if (code.equals(value.getCode())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDescription() {
        return description;
    }


}
