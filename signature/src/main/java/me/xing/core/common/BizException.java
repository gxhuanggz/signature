package me.xing.core.common;

import lombok.Data;


@Data
public class BizException extends RuntimeException {

    /**
     * 错误码
     */
    private ErrorCode errorCode;

    /**
     * 无参默认构造UNSPECIFIED
     */
    public BizException() {
        super(BizErrorCode.UNSPECIFIED.getDescription());
        this.errorCode = BizErrorCode.UNSPECIFIED;
    }

    /**
     * 指定错误码构造通用异常
     *
     * @param errorCode 错误码
     */
    public BizException(ErrorCode errorCode) {
        super(errorCode.getDescription());
        this.errorCode = errorCode;
    }

    /**
     * 指定详细描述构造通用异常
     *
     * @param message 详细描述
     */
    public BizException(String message) {
        super(message);
        this.errorCode = BizErrorCode.UNSPECIFIED;
    }

    /**
     * 指定导火索构造通用异常
     *
     * @param t 导火索
     */
    public BizException(Throwable t) {
        super(t);
        this.errorCode = BizErrorCode.UNSPECIFIED;
    }

    /**
     * 构造通用异常
     *
     * @param errorCode 错误码
     * @param message   详细描述
     */
    public BizException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * 构造通用异常
     *
     * @param errorCode 错误码
     * @param t         导火索
     */
    public BizException(ErrorCode errorCode, Throwable t) {
        super(errorCode.getDescription(), t);
        this.errorCode = errorCode;
    }

    /**
     * 构造通用异常
     *
     * @param message 详细描述
     * @param t       导火索
     */
    public BizException(String message, final Throwable t) {
        super(message, t);
        this.errorCode = BizErrorCode.UNSPECIFIED;
    }

    /**
     * 构造通用异常
     *
     * @param errorCode 错误码
     * @param message   详细描述
     * @param t         导火索
     */
    public BizException(ErrorCode errorCode, String message,
                        Throwable t) {
        super(message, t);
        this.errorCode = errorCode;
    }


}