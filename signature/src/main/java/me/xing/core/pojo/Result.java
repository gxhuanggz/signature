package me.xing.core.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "基础对象")
public class Result<T> {


    @ApiModelProperty(value = "状态码， 200:success")
    private int code = 200;

    @ApiModelProperty(value = "返回的实际内容")
    private T data;

    @ApiModelProperty(value = "返回信息")
    private String msg = "success";

    public Result() {

    }

    public Result(int code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public static <T> Result ok() {
        return new Result();
    }

    public static <T> Result ok(T data) {
        Result r = new Result();
        r.setData(data);
        return r;
    }

    public static <T> Result ok(String message) {
        Result r = new Result();
        r.setMsg(message);
        return r;
    }

    public static <T> Result<T> error(String msg) {
        return error(500, msg);
    }

    public static <T> Result<T> error(int code, String msg) {
        Result<T> r = new Result<>();
        r.setCode(code);
        r.setMsg(msg);
        return r;
    }


}
